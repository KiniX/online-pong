import org.lwjgl.opengl.GL11;


public class Shape
{
	private static float r, g, b;
	
	public static void init(int color)
	{
		switch(color)
		{
		case 0:
			r = 1;
			g = 1;
			b = 1;
			break;
		case 1:
			r = 0;
			g = 1;
			b = 0;
			break;
		case 2:
			r = 0;
			g = 0;
			b = 1;
			break;
		case 3:
			r = 1;
			g = 0;
			b = 0;
			break;
		case 4:
			r = 1;
			g = 1;
			b = 0;
			break;
		}
		
		
		GL11.glColor3f(r, g, b);
	}
	
	public static void drawRectangle(int x, int y, int w, int h)
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);  
		
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex2f(x,y);
		GL11.glVertex2f(x+w,y);
		GL11.glVertex2f(x+w,y+h);
		GL11.glVertex2f(x,y+h);
		GL11.glEnd();	
		
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);  
	}
}
