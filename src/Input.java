import org.lwjgl.input.Controllers;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;


public class Input
{
	private float value = 0;
	public boolean running = true;
	private int type = -1;
	
	public Input(int control)
	{
		switch(type = control)
		{
		case 0:
			new KeyboardThread().start();
			break;
		case 1:
			new MouseThread().start();
			break;
		case 2:
			new WheelThread().start();
			break;
		case 3:
			new DpadThread().start();
			break;
		case 4:
			new AnalogThread().start();
			break;
		}
	}
	
	public float getValue()
	{
		if(type == 1 || type == 2)
		{
			float result = value*3;
			value = 0;
			return result;
		}
		else
			return value;
	}

	private class KeyboardThread extends Thread
	{
		public void run()
		{
			while(running)
			{
				if(Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_UP))
				{
					value = -1;
				}
				else if(Keyboard.isKeyDown(Keyboard.KEY_S) || Keyboard.isKeyDown(Keyboard.KEY_DOWN))
				{
					value = 1;						
				}
				else
				{
					value = 0;
				}
				
				Utility.sleep(20);
			}
		}
	}
	
	private class MouseThread extends Thread
	{		
		public void run()
		{
			int prev = Mouse.getY(); 
			int current = Mouse.getY(); 
			
			while(running)
			{
				prev = current;
				current = Mouse.getY();	
				
				value += (prev - current)/20.0;
			}
		}
	}
	
	private class WheelThread extends Thread
	{		
		public void run()
		{
			int wheel;
			while(running)
			{
				wheel = Mouse.getDWheel();
				
				value -= wheel/420.0;
				
				Utility.sleep(20);
			}
		}
	}
	
	private class DpadThread extends Thread
	{
		public void run()
		{
			Utility.createController();
			
			while(running)
			{
				Controllers.poll();
				
				if(Controllers.next())
					value = Controllers.getEventSource().getPovY();
				
				Utility.sleep(20);
			}
		}
	}
	
	private class AnalogThread extends Thread
	{
		public void run()
		{
			Utility.createController();
			
			while(running)
			{
				Controllers.poll();
				
				if(Controllers.next())
					value = Controllers.getEventSource().getYAxisValue();
				
				Utility.sleep(20);
			}
		}
	}
}
