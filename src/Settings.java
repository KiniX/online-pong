import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


public class Settings extends JFrame implements ActionListener
{

	private static final long serialVersionUID = 4264839381574707445L;
	
	// IP stuff
	private final String[] ipExternal = new String[1];
	private Thread ipThread = new Thread(new Runnable() {
		public void run()
		{
			ipExternal[0] = Utility.getIpAddress();
		}
	});
	
	// Settings variables
	public int width, height, difficulty, color, control, sens;
	public boolean online;
	
	// Options
	JComboBox<String> resOption;
	JComboBox<String> colorOption;
	JComboBox<String> controlOption;
	JSlider sensOption;
	JTabbedPane tabPanel;
	JComboBox<String> difficultyOption;
	JTextField IPtoJoin;
	
	public Settings()
	{
		// Get external IP
		ipThread.run();
		
		// Start: Reading settings file
		
		int options[] = new int[4];
		
		File file = new File("options.knx");
		if(file.exists())
		{
			try
			{
				FileInputStream stream = new FileInputStream(file);
				for(int i=0; i<4; i++)
					options[i] = stream.read();
				stream.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			options[0] = 2;
			options[1] = 0;
			options[2] = 0;
			options[3] = 4;
		}
		
		// End: Reading settings file
		
		setLayout(null);
		
		
		// Start: Settings menu
		
		JPanel line = new JPanel()
		{ 
			private static final long serialVersionUID = 6040396881065094824L;

			public void paintComponent(Graphics g)
		    {
				g.setColor(Color.GRAY);
				g.drawLine(350, 10, 350, 240);
		    }
		};
		
		add(line);
		line.setBounds(0, 0, 600, 400);
		
		JLabel resLabel = new JLabel("Resolution :");
		resOption = new JComboBox<String>();
		resOption.addItem("640x400");
		resOption.addItem("800x500");
		resOption.addItem("960x600");
		resOption.addItem("1280x800");
		resOption.addItem("1600x1000");
		resOption.setSelectedIndex(options[0]);
		
		add(resLabel);
		resLabel.setBounds(368, 20, 100, 30);
		add(resOption);
		resOption.setBounds(440, 20, 130, 30);
		
		JLabel colorLabel = new JLabel("Color :");
		colorOption = new JComboBox<String>();
		colorOption.addItem("White");
		colorOption.addItem("Green");
		colorOption.addItem("Blue");
		colorOption.addItem("Red");
		colorOption.addItem("Yellow");
		colorOption.setSelectedIndex(options[1]);
		
		add(colorLabel);
		colorLabel.setBounds(399, 60, 100, 30);
		add(colorOption);
		colorOption.setBounds(440, 60, 130, 30);
		
		JLabel controlLabel = new JLabel("Control :");
		controlOption = new JComboBox<String>();
		controlOption.addItem("Keyboard");
		controlOption.addItem("Mouse Movement");
		controlOption.addItem("Mouse Wheel");
		controlOption.addItem("Gamepad (Dpad)");
		controlOption.addItem("Gamepad (Analog)");
		controlOption.setSelectedIndex(options[2]);
		
		add(controlLabel);
		controlLabel.setBounds(390, 150, 100, 30);
		add(controlOption);
		controlOption.setBounds(440, 150, 130, 30);
		
		JLabel sensLabel = new JLabel("Sensitivity :");
		sensOption = new JSlider(1, 10);
		sensOption.setMinorTickSpacing(1);
		sensOption.setMajorTickSpacing(0);
		sensOption.setPaintTicks(true);
		sensOption.setPaintLabels(true);
		sensOption.setValue(options[3]);
		
		add(sensLabel);
		sensLabel.setBounds(380, 190, 100, 30);
		add(sensOption);
		sensOption.setBounds(440, 185, 130, 40);
		
		// End: Settings menu
		
		// Start: Game menu
		
		tabPanel = new JTabbedPane();
		JPanel singlePanel = new JPanel();
		JPanel hostPanel = new JPanel();
		JPanel joinPanel = new JPanel();
		
		tabPanel.addTab("  Single Player  ", singlePanel);
		tabPanel.addTab("  Host Game  ", hostPanel);
		tabPanel.addTab("  Join Game  ", joinPanel);
		
		singlePanel.setLayout(null);
		hostPanel.setLayout(null);
		joinPanel.setLayout(null);
		
		add(tabPanel);
		tabPanel.setBounds(20, 20, 310, 200);
		
		JLabel difficultyLabel = new JLabel("Difficulty :");
		difficultyOption = new JComboBox<String>();
		difficultyOption.addItem("Easy");
		difficultyOption.addItem("Medium");
		difficultyOption.addItem("Hard");
		difficultyOption.addItem("Insane");
		difficultyOption.setSelectedIndex(1);
		
		singlePanel.add(difficultyLabel);
		difficultyLabel.setBounds(70, 50, 70, 30);
		singlePanel.add(difficultyOption);
		difficultyOption.setBounds(130, 50, 100, 30);
		
		JLabel joinIP = new JLabel("Host IP: ");
		JLabel infoJoin = new JLabel("You need the IP of host. Ask your opponent.");
		IPtoJoin = new JTextField();
		JButton pasteClipboard = new JButton("Paste");
		IPtoJoin.setBounds(90, 50, 160, 30);
		joinIP.setBounds(40, 50, 70, 30);
		infoJoin.setBounds(27, 20, 250, 30);
		pasteClipboard.setBounds(110, 90, 80, 30);
		joinPanel.add(IPtoJoin);
		joinPanel.add(joinIP);
		joinPanel.add(infoJoin);
		joinPanel.add(pasteClipboard);
		
		
		JLabel ipLabel = new JLabel("Your IP address: " + ipExternal[0]);
		JButton startGame = new JButton("Start Game");
		JButton hostGame = new JButton("Wait Player");
		JButton joinGame = new JButton("Join Host");
		JButton copyClipboard = new JButton("Copy To Clipboard");
		singlePanel.add(startGame);
		hostPanel.add(ipLabel);
		hostPanel.add(hostGame);
		joinPanel.add(joinGame);
		hostPanel.add(copyClipboard);
		startGame.setBounds(100, 130, 100, 30);
		ipLabel.setBounds(60, 20, 250, 30);
		copyClipboard.setBounds(80, 50, 140, 30);
		hostGame.setBounds(100, 130, 100, 30);
		joinGame.setBounds(100, 130, 100, 30);
		
		startGame.addActionListener(this);
		copyClipboard.addActionListener(this);
		pasteClipboard.addActionListener(this);
		hostGame.addActionListener(this);
		joinGame.addActionListener(this);
		
		// End: Game menu
		
		int screenW = Toolkit.getDefaultToolkit().getScreenSize().width;
		int screenH = Toolkit.getDefaultToolkit().getScreenSize().height;
		
		setBounds(screenW/2-300, screenH/2-150, 600, 290);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		if (arg0.getActionCommand().equals("Copy To Clipboard"))
		{
			Utility.copyToClipboard(ipExternal[0]);
			return;
		}
		else if (arg0.getActionCommand().equals("Paste"))
		{
			IPtoJoin.setText(Utility.pasteFromClipboard());
			return;
		}
		
		// Start: settings file

		File file = new File("options.knx");
		try
		{
			FileOutputStream stream = new FileOutputStream(file);
			stream.write(resOption.getSelectedIndex());
			stream.write(colorOption.getSelectedIndex());
			stream.write(controlOption.getSelectedIndex());
			stream.write(sensOption.getValue());
			stream.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		// End: settings file
		
		switch(resOption.getSelectedIndex())
		{
		case 0:
			width = 640;
			height = 400;
			break;
		case 1:
			width = 800;
			height = 500;
			break;
		case 2:
			width = 960;
			height = 600;
			break;
		case 3:
			width = 1280;
			height = 800;
			break;
		case 4:
			width = 1600;
			height = 1000;
			break;
		}
		
		color = colorOption.getSelectedIndex();
		difficulty = difficultyOption.getSelectedIndex();
		control = controlOption.getSelectedIndex();
		sens = sensOption.getValue();
		
		if(tabPanel.getSelectedIndex() == 0)
		{
			online = false;
		}
		else
		{
			online = true;
		}
		
		setVisible(false);
		
		new Window(this);

	}

}
