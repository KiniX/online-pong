import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;


public class Window
{
	Settings settings;
	Input input;
	AIPlayer ai;
	OnlinePlayer p2;
	float player1, player2, ballx, bally, angle, ballSpeed;
	int waitingTime, score1, score2;
	
	public Window(Settings settings)
	{
		this.settings = settings;
		
		try
		{
			Display.setDisplayMode(new DisplayMode(settings.width, settings.height));
			Display.create();
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
		
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);               
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);          
		
    	GL11.glEnable(GL11.GL_BLEND);
    	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL11.glLoadIdentity();
		GL11.glOrtho(0, 1280, 800, 0, 1, -1);
		
		long time = System.currentTimeMillis();
		long difference;
		
		
		player1 = 325;
		player2 = 325;
		ballx = 640;
		bally = 440;
		score1 = 0;
		score2 = 0;
		if(settings.tabPanel.getSelectedIndex()==1)
			angle = 42;
		else
			angle = 360-42;
		ballSpeed = 1;
		Shape.init(settings.color);
		Text.init(settings.color);
		input = new Input(settings.control);
		waitingTime = 100;
		
		if(settings.online)
		{
			p2 = new OnlinePlayer(this, settings.tabPanel.getSelectedIndex());
		}
		else
		{
			ai = new AIPlayer(this);
		}
		
		if(settings.online)
		{
			int waiting = 42;
			int dot = 0;
			while(p2.partnerIP == null && !Display.isCloseRequested()) // TODO: close request
			{
				difference = System.currentTimeMillis() - time;
				
				if(difference<33)
					Utility.sleep(33-difference);
				
				time = System.currentTimeMillis();
				
				if(waiting-- < 0)
				{
					if(dot++>2)
						dot = 0;
					waiting = 42;
				}
				
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
				switch(dot)
				{
				case 0:
					Text.draw2(300, 300, "Waiting for other player");
					break;
				case 1:
					Text.draw2(300, 300, "Waiting for other player.");
					break;
				case 2:
					Text.draw2(300, 300, "Waiting for other player..");
					break;
				case 3:
					Text.draw2(300, 300, "Waiting for other player...");
					break;
				}
				Display.update();
			}
		}
				
		
		while(!Display.isCloseRequested())
		{
			difference = System.currentTimeMillis() - time;
			
			if(difference<33)
				Utility.sleep(33-difference);
			
			time = System.currentTimeMillis();
				
			if(waitingTime == 0)
				moveBall();
			else
				waitingTime--;
			
			controlPlayer1();
			controlPlayer2();
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
			draw();
				
			Display.update();
		}
		
		input.running = false;
		if(settings.online)
			p2.running = false;
		
		Utility.sleep(100);
		
		Display.destroy();
		System.exit(1);
	}
	
	
	private void moveBall()
	{
		ballSpeed *= 1.001;
		
		if(ballx == 50)
		{
			if(bally-player1 < 0 || bally-player1 > 200)
			{
				reset();
				score2++;
			}
		}
		else if(ballx == 1220)
		{
			if(bally-player2 < 0  || bally-player2 > 200)
			{
				reset();
				score1++;
			}
		}
		
		ballx += Math.cos(Math.PI*angle/180.0)*10*ballSpeed;
		bally += Math.sin(Math.PI*angle/180.0)*10*ballSpeed;
		
		if(ballx ==50)
		{
			// do nothing
		}
		else if(ballx < 50+ballSpeed)
		{
			ballx = 50;
		}
		else if(ballx == 1220)
		{
			// do nothing
		}
		else if(ballx > 1220-ballSpeed)
		{
			ballx = 1220;
		}
		
		if(bally == 760)
		{
			// do nothing
		}
		else if(bally > 760-ballSpeed)
		{
			bally = 760;
		}
		else if(bally == 110)
		{
			// do nothing
		}
		else if(bally < 110+ballSpeed)
		{
			bally = 110;
		}
		
		if(ballx == 50)
		{
			angle += (90-angle)*2; // TODO: a��
		}
		else if(ballx == 1220)
		{
			angle += (90-angle)*2; // TODO: a��
		}
		
		if(bally == 110 || bally == 760)
		{
			angle += (180-angle)*2;
		}
		
		if(angle>360)
			angle-=360;
		
	}
	
	
	private void controlPlayer1()
	{
		float movement = input.getValue()*settings.sens*8;
		
		if(movement < 0)
		{
			if(player1 > 104-movement)
				player1 += movement;
			else
				player1 = 104;
		}
		else if(movement > 0)
		{
			if(player1 < 566-movement)
				player1 += movement;	
			else
				player1 = 566;
		}
	}
	
	private void controlPlayer2()
	{
		if(settings.online)
		{
			p2.action();
		}
		else
		{
			ai.action();
		}
	}
	
	private void draw()
	{
		// Draw border
		Shape.drawRectangle(16, 96, 1248, 4);
		Shape.drawRectangle(1260, 96, 4, 678);
		Shape.drawRectangle(16, 770, 1248, 4);
		Shape.drawRectangle(16, 96, 4, 678);
		
		// Draw player1
		Shape.drawRectangle(30, (int)player1, 20, 200);
		
		// Draw player2
		Shape.drawRectangle(1230, (int)player2, 20, 200);
		
		// Draw ball
		Shape.drawRectangle((int)ballx-15, (int)bally-7, 30, 14);
		Shape.drawRectangle((int)ballx-11, (int)bally-11, 22, 22);
		Shape.drawRectangle((int)ballx-7, (int)bally-15, 14, 30);
		
		// Draw score
		Text.draw(500, 13, String.format("%02d", score1) + " - " + String.format("%02d", score2));
	}
	
	private void reset()
	{
		player1 = 325;
		player2 = 325;
		ballx = 640;
		bally = 440;
		if(settings.tabPanel.getSelectedIndex()==1)
			angle = 42;
		else
			angle = 360-42;
		ballSpeed = 1;
		waitingTime =42;
	}
}
