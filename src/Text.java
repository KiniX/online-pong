import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

public class Text
{
	private static Font awtFont, awtFont2;
	private static TrueTypeFont font, font2;
	private static Color color;

	public static void init(int mainColor)
	{
		try
		{
			awtFont = Font.createFont(Font.TRUETYPE_FONT, new File("font.ttf")).deriveFont(90f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("font.ttf")));

			awtFont2 = Font.createFont(Font.TRUETYPE_FONT, new File("font.ttf")).deriveFont(50f);
            GraphicsEnvironment ge2 = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge2.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("font.ttf")));
		}
		catch (FontFormatException e)
		{
			JOptionPane.showMessageDialog(null, "Font file is missing or corrupted.", "Font error", JOptionPane.ERROR_MESSAGE);
			awtFont = new Font("Times New Roman", Font.BOLD, 67);
			awtFont2 = new Font("Times New Roman", Font.BOLD, 37);
		}
		catch (IOException e)
		{
			JOptionPane.showMessageDialog(null, "Font file is missing or corrupted.", "Font error", JOptionPane.ERROR_MESSAGE);
			awtFont = new Font("Times New Roman", Font.BOLD, 67);
			awtFont2 = new Font("Times New Roman", Font.BOLD, 37);
		}
		
		font = new TrueTypeFont(awtFont, false);
		font2 = new TrueTypeFont(awtFont2, false);
		
		float r = 0, g = 0, b = 0;
		switch(mainColor)
		{
		case 0:
			r = 1;
			g = 1;
			b = 1;
			break;
		case 1:
			r = 0;
			g = 1;
			b = 0;
			break;
		case 2:
			r = 0;
			g = 0;
			b = 1;
			break;
		case 3:
			r = 1;
			g = 0;
			b = 0;
			break;
		case 4:
			r = 1;
			g = 1;
			b = 0;
			break;
		}
		
		color = new Color(r,g,b);
	}
	
	public static void draw(float x, float y, String str)
	{	
		
		font.drawString(x,y, str, color);
	}
	
	public static void draw2(float x, float y, String str)
	{	
	
		font2.drawString(x,y, str, color);
	}


}
