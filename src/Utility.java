import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import org.lwjgl.input.Controllers;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.URL;


public enum Utility
{
	;
	
	/** Checks the controllers collection and calls 
	 * <code>org.lwjgl.input.Controllers.create()</code> if the collection is 
	 * not initialized.
	 */
	public static void createController()
	{
		try 
		{
			if (!Controllers.isCreated())
				Controllers.create();
		} 
		catch (org.lwjgl.LWJGLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/** 
	 *	Calls <code>java.lang.Thread.sleep(long)</code>
	 *	@param millis - the length of time to sleep in milliseconds
	 *	@see Thread
	 */
	public static void sleep(long millis)
	{
		try 
		{
			Thread.sleep(millis);
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/** Copies a string to clipboard.
	 * @param str - string to be copied to clipboard.
	 */
	public static void copyToClipboard(String str)
	{
		StringSelection stringSelection = new StringSelection(str);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);
	}
	
	/** Returns the clipboard data.
	 * @param str - string to be returned.
	 */
	public static String pasteFromClipboard()
	{
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		String val = null;
		
		try 
		{
			val = (String) clipboard.getContents(null)
					.getTransferData(DataFlavor.stringFlavor);
		}
		catch (UnsupportedFlavorException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return val;
	}
	
	/** Returns external IP address. 
	 *	<a href="http://stackoverflow.com/a/16882418">Source</a>
	 *	@return External IP as a string.
	 */
	public static String getIpAddress() 
	{
		URL myIP;
		try 
		{
			myIP = new URL("http://api.externalip.net/ip/");

			BufferedReader in = new BufferedReader(
					new InputStreamReader(myIP.openStream()));
			return in.readLine();
		} 
		catch (Exception e) 
		{
			try 
			{
				myIP = new URL("http://myip.dnsomatic.com/");

				BufferedReader in = new BufferedReader(
						new InputStreamReader(myIP.openStream()));
				return in.readLine();
			} 
			catch (Exception e1) 
			{
				try 
				{
					myIP = new URL("http://icanhazip.com/");

					BufferedReader in = new BufferedReader(
							new InputStreamReader(myIP.openStream()));
					return in.readLine();
				} 
				catch (Exception e2) 
				{
					e2.printStackTrace(); 
				}
			}
		}

	    return null;
	}
}
