import java.util.Random;


public class AIPlayer
{
	Window main;
	Random rand;
	int direction = 0;
	
	public AIPlayer(Window main)
	{
		this.main = main;
		rand = new Random();
	}
	
	public void action()
	{
		if(main.ballx < 542)
		{
			if(main.settings.difficulty > 1)
				if(main.player2 > 327)
					direction = -1;
				else if(main.player2 <323)
					direction = 1;
				else
					direction = 0;		
		}
		else
		{
			if(rand.nextInt() % (42 - 6*main.settings.difficulty) < 2) // TODO: difficulty
			{
				if(main.bally - (main.player2+100) < -10*(7-2*main.settings.difficulty))
				{
					direction = -1;
				}
				else if(main.bally - (main.player2+100) > 10*(7-2*main.settings.difficulty))
				{
					direction = 1;
				}
				else
				{
					direction = 0;
				}
			}
		}
		
		if(direction == -1)
		{
			if(main.player2 > 104+main.ballSpeed*7)
				main.player2-=main.ballSpeed*7;
			else
				main.player2 = 104;
		}
		else if(direction == 1)
		{
			if(main.player2 < 566-main.ballSpeed*7)
				main.player2+=main.ballSpeed*7;
			else
				main.player2 = 566;			
		}
		

	}
}
