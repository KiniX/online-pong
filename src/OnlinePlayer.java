import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;


public class OnlinePlayer
{
	private boolean host;
	private Window main;
	public String partnerIP;
	public boolean running;
	
	private int value;
	
	public OnlinePlayer(Window main, int index)
	{
		this.main = main;
		
		if(index == 1)
		{
			host = false;
		}
		else
		{
			host = true;
			partnerIP = main.settings.IPtoJoin.getText();
		}
		
		running = true;
		new Listen();
		new Send();
	}
	
	public void action()
	{
		main.player2 = value;
	}
	
	private class Listen extends Thread
	{
		public void run()
		{
			if(host)
			{
				ServerSocket serverSocket = null;
		        try
		        {
		            serverSocket = new ServerSocket(4242);
		        }
		        catch (IOException e)
		        {
		            JOptionPane.showMessageDialog(null, "We could not create a connection on port 4242", "Connection Error", JOptionPane.ERROR_MESSAGE);
		        }

		        Socket clientSocket = null;
		        try
		        {
		            clientSocket = serverSocket.accept();
		            partnerIP = clientSocket.getInetAddress().toString();
		            System.out.println(partnerIP);
		        }
		        catch (IOException e)
		        {
		            JOptionPane.showMessageDialog(null, "We could accept the connection on port 4242", "Connection Error", JOptionPane.ERROR_MESSAGE);
		        }
		        
		        try
		        {
					PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			        String inputLine;
			        
			        while ((inputLine = in.readLine()) != null && running)
			        {
			             value = Integer.parseInt(inputLine);
			        }
			        
			        out.close();
			        in.close();
			        clientSocket.close();
			        serverSocket.close();
				}
		        catch (IOException e)
		        {
					e.printStackTrace();
				}
		        
			}
			
		}		
	}
	
	private class Send extends Thread
	{
		public void run()
		{
			while(partnerIP == null);
			
			Socket echoSocket = null;
			PrintWriter out = null;
			BufferedReader in = null;
			
			try
			{
				echoSocket = new Socket(partnerIP, 4242);
				out = new PrintWriter(echoSocket.getOutputStream(), true);
				in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
			}
			catch (UnknownHostException e)
			{
				System.err.println("Don't know about host: localhost.");
				System.exit(1);
			}
			catch (IOException e)
			{
				System.err.println("Couldn't get I/O for the connection to: taranis.");
				System.exit(1);
			}

			try
			{
				while (running)
				{
					out.println(main.player1);
				}
				
				out.close();
				in.close();
				echoSocket.close();
			}
			catch(Exception e)
			{
				
			}
		}		
	}
}
